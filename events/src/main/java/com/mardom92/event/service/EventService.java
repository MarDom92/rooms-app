package com.mardom92.event.service;

import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.ResponseMessageInfo;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EventService {

    List<EventDTO> getEvents();

    EventDTO getEventById(Long id);

    ResponseEntity<ResponseMessageInfo> createEvent(EventDTO event);

    ResponseEntity<ResponseMessageInfo> deleteEvent(Long id);

    EventDTO putEvent(Long id, EventDTO eventDTO);

    EventDTO patchEvent(Long id, EventDTO eventDTO);
}

package com.mardom92.event.service;

import com.mardom92.event.exception.EventError;
import com.mardom92.event.exception.EventException;
import com.mardom92.event.mapper.EventMapper;
import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.EventEntity;
import com.mardom92.event.model.ResponseMessageInfo;
import com.mardom92.event.model.RoomDTO;
import com.mardom92.event.repository.EventRepository;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final RoomServiceClient roomServiceClient;
    private final EventRepository eventRepository;
    private final EventMapper eventMapper;

    @Override
    public List<EventDTO> getEvents() {
        List<EventEntity> events = eventRepository.findAll();
        return events.stream()
                .map(this::getEventDtoWithRoomName)
                .toList();
    }

    @Override
    public EventDTO getEventById(Long id) {
        EventEntity eventEntity = getEventEntity(id);
        return getEventDtoWithRoomName(eventEntity);
    }

    @Override
    public ResponseEntity<ResponseMessageInfo> createEvent(EventDTO eventDTO) {
        validateEventNameExists(eventDTO);

        EventEntity eventToSave = eventMapper.dtoToEntity(eventDTO);
        setRoomIdInEventByRoomName(eventToSave, eventDTO.getRoomName());
        eventRepository.save(eventToSave);

        return ResponseEntity.ok().body(new ResponseMessageInfo("Poprawnie dodano nowe wydarzenie '" + eventDTO.getName() + "'"));
    }

    @Override
    public ResponseEntity<ResponseMessageInfo> deleteEvent(Long id) {
        EventEntity eventToDelete = getEventEntity(id);
        eventRepository.delete(eventToDelete);
        return ResponseEntity.ok().body(new ResponseMessageInfo("Poprawnie usunięto istniejące wydarzenie o id = '" + id + "'"));
    }

    @Override
    public EventDTO putEvent(Long id, EventDTO eventDTO) {
        EventEntity eventToUpdate = getEventEntity(id);
        checkIfRoomExist(eventDTO.getRoomName());
        setEventFields(eventDTO, eventToUpdate);
        setRoomIdInEventByRoomName(eventToUpdate, eventDTO.getRoomName());
        eventRepository.save(eventToUpdate);

        EventDTO resultDto = eventMapper.entityToDto(eventToUpdate);
        resultDto.setRoomName(eventDTO.getRoomName());
        return resultDto;
    }

    @Override
    public EventDTO patchEvent(Long id, EventDTO eventDTO) {
        EventEntity eventToUpdate = getEventEntity(id);
        checkIfRoomExist(eventDTO.getRoomName());
        validateEventFields(eventDTO, eventToUpdate);
        eventRepository.save(eventToUpdate);

        EventDTO resultDto = eventMapper.entityToDto(eventToUpdate);
        resultDto.setRoomName(eventDTO.getRoomName());
        return resultDto;
    }

    private EventDTO getEventDtoWithRoomName(EventEntity eventEntity) {
        RoomDTO roomDTO = roomServiceClient.getRoomById(eventEntity.getRoomId());
        EventDTO eventDTO = eventMapper.entityToDto(eventEntity);
        eventDTO.setRoomName(roomDTO.getName());
        return eventDTO;
    }

    private EventEntity getEventEntity(Long id) {
        return eventRepository.findById(id)
                .orElseThrow(() -> new EventException(EventError.EVENT_NOT_FOUND));
    }

    private void validateEventNameExists(EventDTO event) {
        if (eventRepository.existsByName(event.getName())) {
            throw new EventException(EventError.EVENT_NAME_ALREADY_EXIST);
        }
    }

    private void validateEventFields(EventDTO eventDTO, EventEntity eventToUpdate) {
        if (Objects.nonNull(eventDTO.getName())) {
            eventToUpdate.setName(eventDTO.getName());
        }
        if (Objects.nonNull(eventDTO.getStartDate())) {
            if (Objects.nonNull(eventDTO.getEndDate())
                    && eventDTO.getStartDate().isAfter(eventDTO.getEndDate())) {
                throw new EventException(EventError.EVENT_END_DATE_IS_BEFORE_START_DATE);
            }
            if (eventDTO.getStartDate().isAfter(eventToUpdate.getEndDate())) {
                throw new EventException(EventError.EVENT_END_DATE_IS_BEFORE_START_DATE);
            }
            eventToUpdate.setStartDate(eventDTO.getStartDate());
        }
        if (Objects.nonNull(eventDTO.getEndDate())) {
            if (Objects.nonNull(eventDTO.getStartDate())
                    && eventDTO.getEndDate().isBefore(eventDTO.getStartDate())) {
                throw new EventException(EventError.EVENT_END_DATE_IS_BEFORE_START_DATE);
            }
            if (eventDTO.getEndDate().isBefore(eventToUpdate.getStartDate())) {
                throw new EventException(EventError.EVENT_END_DATE_IS_BEFORE_START_DATE);
            }
            eventToUpdate.setEndDate(eventDTO.getEndDate());
        }
        if (Objects.nonNull(eventDTO.getRoomName())) {
            setRoomIdInEventByRoomName(eventToUpdate, eventDTO.getRoomName());
        }
    }

    private void setEventFields(EventDTO eventDTO, EventEntity eventToUpdate) {
        eventToUpdate.setName(eventDTO.getName());
        eventToUpdate.setStartDate(eventDTO.getStartDate());
        eventToUpdate.setEndDate(eventDTO.getEndDate());
    }

    private void setRoomIdInEventByRoomName(EventEntity event, String name) {
        Long roomId;
        try {
            roomId = roomServiceClient.getRoomIdByName(name);
        } catch (FeignException exception) {
            throw new EventException(EventError.EVENT_WITH_THAT_ROOM_NOT_FOUND);
        }
        event.setRoomId(roomId);
    }

    private void checkIfRoomExist(String name) {
        if (!roomServiceClient.isRoomExist(name)) {
            throw new EventException(EventError.EVENT_WITH_THAT_ROOM_NOT_FOUND);
        }
    }
}

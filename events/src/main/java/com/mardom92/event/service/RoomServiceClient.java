package com.mardom92.event.service;

import com.mardom92.event.model.RoomDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "room-service", url = "${application.config.rooms-url}")
public interface RoomServiceClient {

    @GetMapping("/name/{roomName}")
    Long getRoomIdByName(@PathVariable String roomName);

    @GetMapping("/exist/{roomName}")
    boolean isRoomExist(@PathVariable String roomName);

    @GetMapping("/{id}")
    RoomDTO getRoomById(@PathVariable Long id);
}

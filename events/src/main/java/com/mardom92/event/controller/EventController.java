package com.mardom92.event.controller;

import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.ResponseMessageInfo;
import com.mardom92.event.service.EventService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/events")
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @GetMapping
    public List<EventDTO> getEvents() {
        return eventService.getEvents();
    }

    @GetMapping("/{id}")
    public EventDTO getEventById(@PathVariable Long id) {
        return eventService.getEventById(id);
    }

    @PostMapping
    public ResponseEntity<ResponseMessageInfo> createEvent(@RequestBody @Valid EventDTO event) {
        return eventService.createEvent(event);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessageInfo> deleteEvent(@PathVariable Long id) {
        return eventService.deleteEvent(id);
    }

    @PutMapping("/{id}")
    public EventDTO putEvent(@PathVariable Long id, @RequestBody @Valid EventDTO eventDTO) {
        return eventService.putEvent(id, eventDTO);
    }

    @PatchMapping("/{id}")
    public EventDTO patchEvent(@PathVariable Long id, @RequestBody @Valid EventDTO eventDTO) {
        return eventService.patchEvent(id, eventDTO);
    }
}

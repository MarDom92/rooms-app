package com.mardom92.event.repository;

import com.mardom92.event.model.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<EventEntity, Long> {

    boolean existsByName(String name);
}

package com.mardom92.event.exception;

import com.mardom92.event.model.ResponseMessageInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EventExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ResponseMessageInfo> handleException(EventException exception) {

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (EventError.EVENT_NOT_FOUND.equals(exception.getEventError())) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (EventError.EVENT_WITH_THAT_ROOM_NOT_FOUND.equals(exception.getEventError())) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (EventError.EVENT_NAME_ALREADY_EXIST.equals(exception.getEventError())) {
            httpStatus = HttpStatus.CONFLICT;
        } else if (EventError.EVENT_END_DATE_IS_BEFORE_START_DATE.equals(exception.getEventError())) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return ResponseEntity.status(httpStatus).body(new ResponseMessageInfo(exception.getEventError().getErrorMessage()));
    }
}

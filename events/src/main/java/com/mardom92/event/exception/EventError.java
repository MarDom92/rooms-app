package com.mardom92.event.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EventError {

    EVENT_NOT_FOUND("Nie znaleziono takiego wydarzenia."),
    EVENT_WITH_THAT_ROOM_NOT_FOUND("Nie można przypisać do wydarzenia pokoju o nieistniejącej nazwie."),
    EVENT_NAME_ALREADY_EXIST("Wydarzenie o takiej nazwie już istnieje."),
    EVENT_END_DATE_IS_BEFORE_START_DATE("Data końca wydarzenia jest przed datą rozpoczęcia.");

    private final String errorMessage;
}

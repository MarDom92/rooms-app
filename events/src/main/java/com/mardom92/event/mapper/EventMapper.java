package com.mardom92.event.mapper;

import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.EventEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EventMapper {

    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "roomId")
    EventEntity dtoToEntity(EventDTO eventDTO);

    EventDTO entityToDto(EventEntity eventEntity);
}

package com.mardom92.event.model;

import lombok.Builder;

@Builder
public record ResponseMessageInfo(String message) {
}

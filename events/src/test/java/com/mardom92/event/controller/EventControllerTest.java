package com.mardom92.event.controller;

import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.ResponseMessageInfo;
import com.mardom92.event.service.EventService;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringJUnitConfig
@WebMvcTest(EventController.class)
class EventControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    void init(@Autowired MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void testGetEvents() throws Exception {
        EventDTO event1 = EventDTO.builder()
                .name("Event 1")
                .startDate(Instant.parse("2023-01-01T00:00:00Z"))
                .endDate(Instant.parse("2023-01-02T00:00:00Z"))
                .roomName("Room 1")
                .build();

        EventDTO event2 = EventDTO.builder()
                .name("Event 2")
                .startDate(Instant.parse("2023-01-02T00:00:00Z"))
                .endDate(Instant.parse("2023-01-03T00:00:00Z"))
                .roomName("Room 2")
                .build();

        List<EventDTO> events = Arrays.asList(event1, event2);

        when(eventService.getEvents()).thenReturn(events);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/events"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Event 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].startDate").value("2023-01-01T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].endDate").value("2023-01-02T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].roomName").value("Room 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Event 2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].startDate").value("2023-01-02T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].endDate").value("2023-01-03T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].roomName").value("Room 2"));
    }

    @Test
    void testGetEventById() throws Exception {
        EventDTO event = EventDTO.builder()
                .name("Event 1")
                .startDate(Instant.parse("2023-01-01T00:00:00Z"))
                .endDate(Instant.parse("2023-01-02T00:00:00Z"))
                .roomName("Room 1")
                .build();

        when(eventService.getEventById(anyLong())).thenReturn(event);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/events/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Event 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2023-01-01T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2023-01-02T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomName").value("Room 1"));
    }

    @Test
    void testCreateEvent() throws Exception {
        ResponseMessageInfo responseMessage = ResponseMessageInfo.builder()
                .message("Poprawnie dodano nowe wydarzenie 'Event 1'")
                .build();

        when(eventService.createEvent(any(EventDTO.class))).thenReturn(ResponseEntity.ok(responseMessage));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Event 1\",\"startDate\":\"2023-01-01T00:00:00Z\",\"endDate\":\"2023-01-02T00:00:00Z\",\"roomName\":\"Room 1\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Poprawnie dodano nowe wydarzenie 'Event 1'"));
    }

    @Test
    void testDeleteEvent() throws Exception {
        ResponseMessageInfo responseMessage = ResponseMessageInfo.builder()
                .message("Poprawnie usunięto istniejące wydarzenie o id = '1'")
                .build();

        when(eventService.deleteEvent(anyLong())).thenReturn(ResponseEntity.ok(responseMessage));

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/events/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Poprawnie usunięto istniejące wydarzenie o id = '1'"));
    }

    @Test
    void testPutEvent() throws Exception {
        EventDTO updatedEvent = EventDTO.builder()
                .name("Event 1 Updated")
                .startDate(Instant.parse("2023-01-01T00:00:00Z"))
                .endDate(Instant.parse("2023-01-02T00:00:00Z"))
                .roomName("Room 2")
                .build();

        when(eventService.putEvent(anyLong(), any(EventDTO.class))).thenReturn(updatedEvent);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/events/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Event 1 Updated\",\"startDate\":\"2023-01-01T00:00:00Z\",\"endDate\":\"2023-01-02T00:00:00Z\",\"roomName\":\"Room 2\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Event 1 Updated"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2023-01-01T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2023-01-02T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomName").value("Room 2"));
    }

    @Test
    void testPatchEvent() throws Exception {
        EventDTO updatedEvent = EventDTO.builder()
                .name("Event 1")
                .startDate(Instant.parse("2023-01-01T00:00:00Z"))
                .endDate(Instant.parse("2023-01-03T00:00:00Z"))
                .roomName("Room 1")
                .build();

        when(eventService.patchEvent(anyLong(), any(EventDTO.class))).thenReturn(updatedEvent);

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/events/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"endDate\":\"2023-01-03T00:00:00Z\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Event 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2023-01-01T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2023-01-03T00:00:00Z"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roomName").value("Room 1"));
    }
}
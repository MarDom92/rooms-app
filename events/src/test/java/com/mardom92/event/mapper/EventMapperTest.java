package com.mardom92.event.mapper;

import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.EventEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class EventMapperTest {

    private EventMapper eventMapper = Mappers.getMapper(EventMapper.class);

    @Test
    void dtoToEntity_shouldMapEventDTOToEventEntity() {
        EventDTO eventDTO = EventDTO.builder()
                .name("Event 1")
                .startDate(Instant.now())
                .endDate(Instant.now().plus(1, ChronoUnit.HOURS))
                .roomName("Room 1")
                .build();

        EventEntity eventEntity = eventMapper.dtoToEntity(eventDTO);

        assertNull(eventEntity.getId());
        assertNull(eventEntity.getRoomId());
        assertEquals(eventDTO.getName(), eventEntity.getName());
        assertEquals(eventDTO.getStartDate(), eventEntity.getStartDate());
        assertEquals(eventDTO.getEndDate(), eventEntity.getEndDate());
    }

    @Test
    void entityToDto_shouldMapEventEntityToEventDTO() {
        EventEntity eventEntity = EventEntity.builder()
                .id(1L)
                .name("Event 1")
                .startDate(Instant.now())
                .endDate(Instant.now().plus(1, ChronoUnit.HOURS))
                .roomId(2L)
                .build();

        EventDTO eventDTO = eventMapper.entityToDto(eventEntity);

        assertEquals(eventEntity.getName(), eventDTO.getName());
        assertEquals(eventEntity.getStartDate(), eventDTO.getStartDate());
        assertEquals(eventEntity.getEndDate(), eventDTO.getEndDate());
        assertNull(eventDTO.getRoomName());
    }
}

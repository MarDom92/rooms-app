package com.mardom92.event.service;


import com.mardom92.event.exception.EventException;
import com.mardom92.event.mapper.EventMapper;
import com.mardom92.event.model.EventDTO;
import com.mardom92.event.model.EventEntity;
import com.mardom92.event.model.ResponseMessageInfo;
import com.mardom92.event.model.RoomDTO;
import com.mardom92.event.repository.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class EventServiceImplTest {

    @Mock
    private RoomServiceClient roomServiceClient;

    @Mock
    private EventRepository eventRepository;

    @Mock
    private EventMapper eventMapper;

    private EventServiceImpl eventService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        eventService = new EventServiceImpl(roomServiceClient, eventRepository, eventMapper);
    }

    @Test
    void getEvents_shouldReturnListOfEventsWithRoomNames() {
        List<EventEntity> eventEntities = Arrays.asList(
                EventEntity.builder().name("Event 1").roomId(1L).build(),
                EventEntity.builder().name("Event 2").roomId(2L).build()
        );
        List<EventDTO> expectedEvents = Arrays.asList(
                EventDTO.builder().name("Event 1").roomName("Room 1").build(),
                EventDTO.builder().name("Event 2").roomName("Room 2").build()
        );
        when(eventRepository.findAll()).thenReturn(eventEntities);
        when(eventMapper.entityToDto(eventEntities.get(0))).thenReturn(expectedEvents.get(0));
        when(eventMapper.entityToDto(eventEntities.get(1))).thenReturn(expectedEvents.get(1));
        when(roomServiceClient.getRoomById(1L)).thenReturn(RoomDTO.builder().name("Room 1").build());
        when(roomServiceClient.getRoomById(2L)).thenReturn(RoomDTO.builder().name("Room 2").build());

        List<EventDTO> result = eventService.getEvents();

        assertEquals(expectedEvents, result);
        verify(eventRepository, times(1)).findAll();
        verify(eventMapper, times(1)).entityToDto(eventEntities.get(0));
        verify(eventMapper, times(1)).entityToDto(eventEntities.get(1));
        verify(roomServiceClient, times(1)).getRoomById(1L);
        verify(roomServiceClient, times(1)).getRoomById(2L);
    }

    @Test
    void getEventById_shouldReturnEventWithRoomName() {
        Long eventId = 1L;
        EventEntity eventEntity = EventEntity.builder().name("Event 1").roomId(1L).build();
        EventDTO expectedEvent = EventDTO.builder().name("Event 1").roomName("Room 1").build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventEntity));
        when(eventMapper.entityToDto(eventEntity)).thenReturn(expectedEvent);
        when(roomServiceClient.getRoomById(1L)).thenReturn(RoomDTO.builder().name("Room 1").build());

        EventDTO result = eventService.getEventById(eventId);

        assertEquals(expectedEvent, result);
        verify(eventRepository, times(1)).findById(eventId);
        verify(eventMapper, times(1)).entityToDto(eventEntity);
        verify(roomServiceClient, times(1)).getRoomById(1L);
    }

    @Test
    void createEvent_shouldReturnResponseEntityWithSuccessMessage() {
        EventDTO eventDTO = EventDTO.builder().name("New Event").roomName("Room 1").build();
        EventEntity eventToSave = EventEntity.builder().name("New Event").build();
        when(eventMapper.dtoToEntity(eventDTO)).thenReturn(eventToSave);
        when(roomServiceClient.getRoomIdByName("Room 1")).thenReturn(1L);
        when(eventRepository.existsByName("New Event")).thenReturn(false);

        ResponseEntity<ResponseMessageInfo> result = eventService.createEvent(eventDTO);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertEquals("Poprawnie dodano nowe wydarzenie 'New Event'", result.getBody().message());
        verify(eventMapper, times(1)).dtoToEntity(eventDTO);
        verify(roomServiceClient, times(1)).getRoomIdByName("Room 1");
        verify(eventRepository, times(1)).existsByName("New Event");
        verify(eventRepository, times(1)).save(eventToSave);
    }

    @Test
    void createEvent_withExistingEventName_shouldThrowEventException() {
        EventDTO eventDTO = EventDTO.builder().name("Existing Event").roomName("Room 1").build();
        when(eventRepository.existsByName(eventDTO.getName())).thenReturn(true);

        assertThrows(EventException.class, () -> {
            eventService.createEvent(eventDTO);
        });

        verify(eventRepository, times(1)).existsByName(eventDTO.getName());
        verify(eventRepository, never()).save(any());
    }

    @Test
    void deleteEvent_shouldReturnResponseEntityWithSuccessMessage() {
        Long eventId = 1L;
        EventEntity eventToDelete = EventEntity.builder().name("Event 1").build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventToDelete));

        ResponseEntity<ResponseMessageInfo> result = eventService.deleteEvent(eventId);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNotNull(result.getBody());
        assertEquals("Poprawnie usunięto istniejące wydarzenie o id = '1'", result.getBody().message());
        verify(eventRepository, times(1)).findById(eventId);
        verify(eventRepository, times(1)).delete(eventToDelete);
    }

    @Test
    void putEvent_shouldReturnUpdatedEvent() {
        Long eventId = 1L;
        EventDTO eventDTO = EventDTO.builder().name("Updated Event").roomName("Room 2").build();
        EventEntity eventToUpdate = EventEntity.builder().name("Event 1").roomId(1L).build();
        EventDTO expectedEvent = EventDTO.builder().name("Updated Event").roomName("Room 2").build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventToUpdate));
        when(roomServiceClient.isRoomExist("Room 2")).thenReturn(true);
        when(eventMapper.entityToDto(eventToUpdate)).thenReturn(expectedEvent);

        EventDTO result = eventService.putEvent(eventId, eventDTO);

        assertEquals(expectedEvent, result);
        assertEquals(eventDTO.getRoomName(), result.getRoomName());
        verify(eventRepository, times(1)).findById(eventId);
        verify(roomServiceClient, times(1)).isRoomExist("Room 2");
        verify(eventMapper, times(1)).entityToDto(eventToUpdate);
        verify(eventRepository, times(1)).save(eventToUpdate);
    }

    @Test
    void putEvent_withNonExistingRoom_shouldThrowEventException() {
        Long eventId = 1L;
        EventDTO eventDTO = EventDTO.builder().name("Updated Event").roomName("Non Existing Room").build();
        EventEntity eventToUpdate = EventEntity.builder().name("Event 1").roomId(1L).build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventToUpdate));
        when(roomServiceClient.isRoomExist(eventDTO.getRoomName())).thenReturn(false);

        assertThrows(EventException.class, () -> {
            eventService.putEvent(eventId, eventDTO);
        });

        verify(eventRepository, times(1)).findById(eventId);
        verify(roomServiceClient, times(1)).isRoomExist(eventDTO.getRoomName());
        verify(eventRepository, never()).save(any());
    }

    @Test
    void patchEvent_shouldReturnUpdatedEvent() {
        Long eventId = 1L;
        EventDTO eventDTO = EventDTO.builder().name("Updated Event").roomName("Room 2").build();
        EventEntity eventToUpdate = EventEntity.builder().name("Event 1").roomId(1L).build();
        EventDTO expectedEvent = EventDTO.builder().name("Updated Event").roomName("Room 2").build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventToUpdate));
        when(roomServiceClient.isRoomExist("Room 2")).thenReturn(true);
        when(eventMapper.entityToDto(eventToUpdate)).thenReturn(expectedEvent);

        EventDTO result = eventService.patchEvent(eventId, eventDTO);

        assertEquals(expectedEvent, result);
        assertEquals(eventDTO.getRoomName(), result.getRoomName());
        verify(eventRepository, times(1)).findById(eventId);
        verify(roomServiceClient, times(1)).isRoomExist("Room 2");
        verify(eventMapper, times(1)).entityToDto(eventToUpdate);
        verify(eventRepository, times(1)).save(eventToUpdate);
    }

    @Test
    void patchEvent_withNonExistingRoom_shouldThrowEventException() {
        Long eventId = 1L;
        EventDTO eventDTO = EventDTO.builder().name("Updated Event").roomName("Non Existing Room").build();
        EventEntity eventToUpdate = EventEntity.builder().name("Event 1").roomId(1L).build();
        when(eventRepository.findById(eventId)).thenReturn(java.util.Optional.of(eventToUpdate));
        when(roomServiceClient.isRoomExist(eventDTO.getRoomName())).thenReturn(false);

        assertThrows(EventException.class, () -> {
            eventService.patchEvent(eventId, eventDTO);
        });

        verify(eventRepository, times(1)).findById(eventId);
        verify(roomServiceClient, times(1)).isRoomExist(eventDTO.getRoomName());
        verify(eventRepository, never()).save(any());
    }
}

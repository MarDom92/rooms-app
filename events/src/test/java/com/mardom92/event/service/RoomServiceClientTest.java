package com.mardom92.event.service;

import com.mardom92.event.model.RoomDTO;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RoomServiceClientTest {

    private RoomServiceClient roomServiceClient = mock(RoomServiceClient.class);

    @Test
    void getRoomIdByName_existingRoomName_shouldReturnRoomId() {
        String roomName = "Room 1";
        Long expectedRoomId = 1L;
        when(roomServiceClient.getRoomIdByName(roomName)).thenReturn(expectedRoomId);

        Long result = roomServiceClient.getRoomIdByName(roomName);

        assertEquals(expectedRoomId, result);
        verify(roomServiceClient, times(1)).getRoomIdByName(roomName);
    }

    @Test
    void isRoomExist_existingRoomName_shouldReturnTrue() {
        String roomName = "Room 1";
        when(roomServiceClient.isRoomExist(roomName)).thenReturn(true);

        boolean result = roomServiceClient.isRoomExist(roomName);

        assertTrue(result);
        verify(roomServiceClient, times(1)).isRoomExist(roomName);
    }

    @Test
    void getRoomById_existingId_shouldReturnRoomDTO() {
        Long roomId = 1L;
        RoomDTO expectedRoomDTO = RoomDTO.builder()
                .name("Room 1")
                .build();
        ResponseEntity<RoomDTO> responseEntity = ResponseEntity.ok(expectedRoomDTO);
        when(roomServiceClient.getRoomById(roomId)).thenReturn(responseEntity.getBody());

        RoomDTO result = roomServiceClient.getRoomById(roomId);

        assertNotNull(result);
        assertEquals(expectedRoomDTO, result);
        verify(roomServiceClient, times(1)).getRoomById(roomId);
    }
}

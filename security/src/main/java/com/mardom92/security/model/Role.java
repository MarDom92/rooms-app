package com.mardom92.security.model;

public enum Role {
    USER,
    ADMIN
}

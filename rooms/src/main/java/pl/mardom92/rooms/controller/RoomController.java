package pl.mardom92.rooms.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mardom92.rooms.model.ResponseMessageInfo;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.service.RoomService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/rooms")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;

    @GetMapping
    public List<RoomDTO> getRooms() {
        return roomService.getRooms();
    }

    @GetMapping("/name/{name}")
    public Long getRoomIdByName(@PathVariable String name) {
        return roomService.getRoomIdByName(name);
    }

    @GetMapping("/exist/{name}")
    public boolean isRoomExist(@PathVariable String name) {
        return roomService.isRoomExist(name);
    }

    @GetMapping("/{id}")
    public RoomDTO getRoomById(@PathVariable Long id) {
        return roomService.getRoomById(id);
    }

    @PostMapping
    public ResponseEntity<ResponseMessageInfo> createRoom(@RequestBody @Valid RoomDTO room) {
        return roomService.createRoom(room);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessageInfo> deleteRoom(@PathVariable Long id) {
        return roomService.deleteRoom(id);
    }

    @PutMapping("/{id}")
    public RoomDTO putRoom(@PathVariable Long id, @RequestBody @Valid RoomDTO roomDTO) {
        return roomService.putRoom(id, roomDTO);
    }

    @PatchMapping("/{id}")
    public RoomDTO patchRoom(@PathVariable Long id, @RequestBody @Valid RoomDTO roomDTO) {
        return roomService.patchRoom(id, roomDTO);
    }
}

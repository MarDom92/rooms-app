package pl.mardom92.rooms.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.model.RoomEntity;

@Mapper(componentModel = "spring")
public interface RoomMapper {

    @Mapping(ignore = true, target = "id")
    RoomEntity dtoToEntity(RoomDTO roomDTO);

    RoomDTO entityToDto(RoomEntity roomEntity);
}

package pl.mardom92.rooms.model;

public record ResponseMessageInfo(String message) {
}

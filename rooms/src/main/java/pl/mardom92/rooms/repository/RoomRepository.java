package pl.mardom92.rooms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mardom92.rooms.model.RoomEntity;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Long> {

    @Query("SELECT r.id FROM RoomEntity r WHERE r.name = :name")
    Long findIdByName(@Param("name") String name);

    boolean existsByName(String name);
}

package pl.mardom92.rooms.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RoomError {

    ROOM_NOT_FOUND("Nie znaleziono takiej sali"),
    ROOM_NAME_ALREADY_EXIST("Sala o takiej nazwie już istnieje");

    private final String errorMessage;
}

package pl.mardom92.rooms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.mardom92.rooms.model.ResponseMessageInfo;

@RestControllerAdvice
public class RoomExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ResponseMessageInfo> handleException(RoomException exception) {

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (RoomError.ROOM_NOT_FOUND.equals(exception.getRoomError())) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (RoomError.ROOM_NAME_ALREADY_EXIST.equals(exception.getRoomError())) {
            httpStatus = HttpStatus.CONFLICT;
        }

        return ResponseEntity.status(httpStatus).body(new ResponseMessageInfo(exception.getRoomError().getErrorMessage()));
    }
}

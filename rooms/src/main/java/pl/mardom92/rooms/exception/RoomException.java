package pl.mardom92.rooms.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RoomException extends RuntimeException {

    private final RoomError roomError;
}

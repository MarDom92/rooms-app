package pl.mardom92.rooms.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.mardom92.rooms.exception.RoomError;
import pl.mardom92.rooms.exception.RoomException;
import pl.mardom92.rooms.mapper.RoomMapper;
import pl.mardom92.rooms.model.ResponseMessageInfo;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.model.RoomEntity;
import pl.mardom92.rooms.repository.RoomRepository;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;

    @Override
    public List<RoomDTO> getRooms() {
        List<RoomEntity> rooms = roomRepository.findAll();
        return rooms.stream()
                .map(roomMapper::entityToDto)
                .toList();
    }

    @Override
    public RoomDTO getRoomById(Long id) {
        RoomEntity roomEntity = getRoomEntity(id);
        return roomMapper.entityToDto(roomEntity);
    }

    public Long getRoomIdByName(String name) {
        if (roomRepository.existsByName(name)) {
            return roomRepository.findIdByName(name);
        } else {
            throw new RoomException(RoomError.ROOM_NOT_FOUND);
        }
    }

    @Override
    public boolean isRoomExist(String name) {
        return roomRepository.existsByName(name);
    }

    @Override
    public ResponseEntity<ResponseMessageInfo> createRoom(RoomDTO room) {
        validateRoomNameExists(room);
        roomRepository.save(roomMapper.dtoToEntity(room));
        return ResponseEntity.ok().body(new ResponseMessageInfo("Poprawnie dodano nową salę '" + room.getName() + "'"));
    }

    @Override
    public ResponseEntity<ResponseMessageInfo> deleteRoom(Long id) {
        RoomEntity roomEntity = getRoomEntity(id);
        roomRepository.delete(roomEntity);
        return ResponseEntity.ok().body(new ResponseMessageInfo("Poprawnie usunięto istniejącą salę o id = '" + id + "'"));
    }

    @Override
    public RoomDTO putRoom(Long id, RoomDTO roomDTO) {
        getRoomEntity(id);
        RoomEntity updatedRoom = roomMapper.dtoToEntity(roomDTO);
        updatedRoom.setId(id);
        roomRepository.save(updatedRoom);
        return roomMapper.entityToDto(updatedRoom);
    }

    @Override
    public RoomDTO patchRoom(Long id, RoomDTO roomDTO) {
        RoomEntity updatedRoom = getRoomEntity(id);
        if (Objects.nonNull(roomDTO.getName())) {
            updatedRoom.setName(roomDTO.getName());
        }
        if (Objects.nonNull(roomDTO.getStatus())) {
            updatedRoom.setStatus(roomDTO.getStatus());
        }
        roomRepository.save(updatedRoom);
        return roomMapper.entityToDto(updatedRoom);
    }

    private RoomEntity getRoomEntity(Long id) {
        return roomRepository.findById(id)
                .orElseThrow(() -> new RoomException(RoomError.ROOM_NOT_FOUND));
    }

    private void validateRoomNameExists(RoomDTO room) {
        if (roomRepository.existsByName(room.getName())) {
            throw new RoomException(RoomError.ROOM_NAME_ALREADY_EXIST);
        }
    }
}

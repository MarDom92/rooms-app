package pl.mardom92.rooms.service;

import org.springframework.http.ResponseEntity;
import pl.mardom92.rooms.model.ResponseMessageInfo;
import pl.mardom92.rooms.model.RoomDTO;

import java.util.List;

public interface RoomService {

    List<RoomDTO> getRooms();
    
    RoomDTO getRoomById(Long id);

    Long getRoomIdByName(String name);

    boolean isRoomExist(String name);
    
    ResponseEntity<ResponseMessageInfo> createRoom(RoomDTO room);
    
    ResponseEntity<ResponseMessageInfo> deleteRoom(Long id);

    RoomDTO putRoom(Long id, RoomDTO roomDTO);

    RoomDTO patchRoom(Long id, RoomDTO roomDTO);
}

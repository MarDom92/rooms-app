package pl.mardom92.rooms.mapper;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.model.RoomEntity;
import pl.mardom92.rooms.model.Status;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class RoomMapperTest {

    private RoomMapper roomMapper = Mappers.getMapper(RoomMapper.class);

    @Test
    void shouldMapDtoToEntity() {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setName("Test Room");
        roomDTO.setStatus(Status.OCCUPIED);

        RoomEntity roomEntity = roomMapper.dtoToEntity(roomDTO);

        assertNotNull(roomEntity);
        assertEquals(roomDTO.getName(), roomEntity.getName());
        assertEquals(roomDTO.getStatus(), roomEntity.getStatus());
    }

    @Test
    void shouldMapEntityToDto() {
        RoomEntity roomEntity = new RoomEntity();
        roomEntity.setId(1L);
        roomEntity.setName("Test Room");
        roomEntity.setStatus(Status.OCCUPIED);

        RoomDTO roomDTO = roomMapper.entityToDto(roomEntity);

        assertNotNull(roomDTO);
        assertEquals(roomEntity.getName(), roomDTO.getName());
        assertEquals(roomEntity.getStatus(), roomDTO.getStatus());
    }
}

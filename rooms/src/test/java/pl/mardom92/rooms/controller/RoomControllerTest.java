package pl.mardom92.rooms.controller;

import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.mardom92.rooms.model.ResponseMessageInfo;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.service.RoomService;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringJUnitConfig
@WebMvcTest(RoomController.class)
class RoomControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private RoomService roomService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeEach
    void init(@Autowired MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void getRooms_shouldReturnListOfRooms() throws Exception {
        List<RoomDTO> rooms = Arrays.asList(
                RoomDTO.builder()
                        .name("Room 1")
                        .build(),
                RoomDTO.builder()
                        .name("Room 2").
                        build()
        );
        when(roomService.getRooms()).thenReturn(rooms);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/rooms")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(2));
    }

    @Test
    void getRoomById_shouldReturnRoom() throws Exception {
        RoomDTO room = RoomDTO.builder()
                .name("Room 1")
                .build();
        when(roomService.getRoomById(anyLong())).thenReturn(room);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/rooms/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Room 1"));
    }

    @Test
    void getRoomIdByName_shouldReturnRoomId() throws Exception {
        Long roomId = 1L;
        when(roomService.getRoomIdByName(anyString())).thenReturn(roomId);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/rooms/name/Room1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));
    }

    @Test
    void isRoomExist_shouldReturnTrue() throws Exception {
        when(roomService.isRoomExist(anyString())).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/rooms/exist/Room1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    void createRoom_shouldReturnCreatedRoom() throws Exception {
        ResponseMessageInfo response = new ResponseMessageInfo("Poprawnie dodano nową salę 'Room 1'");
        when(roomService.createRoom(any(RoomDTO.class))).thenReturn(ResponseEntity.ok(response));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/rooms")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Room 1\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(response.message()));
    }

    @Test
    void deleteRoom_shouldReturnSuccessMessage() throws Exception {
        ResponseMessageInfo response = new ResponseMessageInfo("Poprawnie usunięto istniejącą salę o id = '1'");
        when(roomService.deleteRoom(anyLong())).thenReturn(ResponseEntity.ok(response));

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/rooms/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(response.message()));
    }

    @Test
    void putRoom_shouldReturnUpdatedRoom() throws Exception {
        RoomDTO updatedRoom = RoomDTO.builder().name("Updated Room 1").build();

        when(roomService.putRoom(anyLong(), any(RoomDTO.class))).thenReturn(updatedRoom);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/rooms/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Updated Room 1\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Updated Room 1"));
    }

    @Test
    void patchRoom_shouldReturnUpdatedRoom() throws Exception {
        RoomDTO updatedRoom = RoomDTO.builder()
                .name("Updated Room 1")
                .build();

        when(roomService.patchRoom(anyLong(), any(RoomDTO.class))).thenReturn(updatedRoom);

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/rooms/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Updated Room 1\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Updated Room 1"));
    }
}

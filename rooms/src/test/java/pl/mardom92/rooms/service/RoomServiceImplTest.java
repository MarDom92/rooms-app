package pl.mardom92.rooms.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import pl.mardom92.rooms.exception.RoomException;
import pl.mardom92.rooms.mapper.RoomMapper;
import pl.mardom92.rooms.model.ResponseMessageInfo;
import pl.mardom92.rooms.model.RoomDTO;
import pl.mardom92.rooms.model.RoomEntity;
import pl.mardom92.rooms.repository.RoomRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class RoomServiceImplTest {

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private RoomMapper roomMapper;

    private RoomServiceImpl roomService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        roomService = new RoomServiceImpl(roomRepository, roomMapper);
    }

    @Test
    void getRooms_shouldReturnListOfRooms() {
        RoomEntity roomEntity = new RoomEntity();
        roomEntity.setId(1L);
        roomEntity.setName("Room 1");
        when(roomRepository.findAll()).thenReturn(Arrays.asList(roomEntity));
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setName("Room 1");
        when(roomMapper.entityToDto(roomEntity)).thenReturn(roomDTO);

        List<RoomDTO> rooms = roomService.getRooms();

        assertEquals(1, rooms.size());
        RoomDTO firstRoom = rooms.get(0);
        assertEquals("Room 1", firstRoom.getName());
        verify(roomRepository, times(1)).findAll();
        verify(roomMapper, times(1)).entityToDto(roomEntity);
    }

    @Test
    void getRoomById_shouldReturnExistingRoom() {
        RoomEntity roomEntity = new RoomEntity();
        roomEntity.setId(1L);
        roomEntity.setName("Room 1");
        when(roomRepository.findById(1L)).thenReturn(Optional.of(roomEntity));
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setName("Room 1");
        when(roomMapper.entityToDto(roomEntity)).thenReturn(roomDTO);

        RoomDTO result = roomService.getRoomById(1L);

        assertNotNull(result);
        assertEquals("Room 1", result.getName());
        verify(roomRepository, times(1)).findById(1L);
        verify(roomMapper, times(1)).entityToDto(roomEntity);
    }

    @Test
    void getRoomById_shouldReturnEmptyNonExistingRoom() {
        when(roomRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RoomException.class, () -> roomService.getRoomById(1L));

        verify(roomRepository, times(1)).findById(1L);
        verifyNoInteractions(roomMapper);
    }

    @Test
    void getRoomIdByName_shouldReturnExistingRoom() {
        when(roomRepository.existsByName("Room 1")).thenReturn(true);
        when(roomRepository.findIdByName("Room 1")).thenReturn(1L);

        Long roomId = roomService.getRoomIdByName("Room 1");

        assertEquals(1L, roomId);
        verify(roomRepository, times(1)).existsByName("Room 1");
        verify(roomRepository, times(1)).findIdByName("Room 1");
    }

    @Test
    void getRoomIdByName_shouldReturnNonExistingRoom() {
        when(roomRepository.existsByName("Room 1")).thenReturn(false);

        assertThrows(RoomException.class, () -> roomService.getRoomIdByName("Room 1"));

        verify(roomRepository, times(1)).existsByName("Room 1");
        verifyNoMoreInteractions(roomRepository);
    }

    @Test
    void isRoomExist_shouldReturnExistingRoom() {
        when(roomRepository.existsByName("Room 1")).thenReturn(true);

        boolean result = roomService.isRoomExist("Room 1");

        assertTrue(result);
        verify(roomRepository, times(1)).existsByName("Room 1");
    }

    @Test
    void isRoomExist_shouldReturnNonExistingRoom() {
        when(roomRepository.existsByName("Room 1")).thenReturn(false);

        boolean result = roomService.isRoomExist("Room 1");

        assertFalse(result);
        verify(roomRepository, times(1)).existsByName("Room 1");
    }

    @Test
    void createRoom_shouldReturnValidRoom() {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setName("Room 1");
        when(roomRepository.existsByName("Room 1")).thenReturn(false);
        RoomEntity savedRoomEntity = new RoomEntity();
        savedRoomEntity.setId(1L);
        when(roomMapper.dtoToEntity(roomDTO)).thenReturn(savedRoomEntity);
        ResponseEntity<ResponseMessageInfo> expectedResponse = ResponseEntity.ok().body(new ResponseMessageInfo("Poprawnie dodano nową salę 'Room 1'"));

        ResponseEntity<ResponseMessageInfo> response = roomService.createRoom(roomDTO);

        assertNotNull(response);
        assertEquals(expectedResponse.getStatusCode(), response.getStatusCode());
        assertEquals(expectedResponse.getBody(), response.getBody());
        verify(roomRepository, times(1)).existsByName("Room 1");
        verify(roomMapper, times(1)).dtoToEntity(roomDTO);
        verify(roomRepository, times(1)).save(savedRoomEntity);
    }

    @Test
    void createRoom_shouldReturnDuplicateRoomName() {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setName("Room 1");
        when(roomRepository.existsByName("Room 1")).thenReturn(true);

        assertThrows(RoomException.class, () -> roomService.createRoom(roomDTO));

        verify(roomRepository, times(1)).existsByName("Room 1");
        verifyNoMoreInteractions(roomRepository);
        verifyNoInteractions(roomMapper);
    }
}
